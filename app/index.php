<?php
session_start();
require_once('view/saveDialogPopup.php');
if ($_SESSION['logged_in']) {

    require_once('view/adminPanel.php');

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['edit-operators'])) {
            require_once('view/editOperators.php');
            exit;
        }
        if (isset($_POST['edit-operators-cansel'])) {
            require_once('view/controlPanel.php');
            exit;
        }
        else {
            require_once('view/controlPanel.php');
        }
    } else {
        require_once('view/controlPanel.php');
    }
}
else {
    require_once('view/login.php');
}