<?php

namespace Api;
use Db\Db;

class ApiController
{
    public function __construct()
    {
        $sort = null;
        $filter = [];
        $data = null;
        $request = $_POST;

        if ($this->is_ajax()) {
            if (isset($request["action"]) && !empty($request["action"])) {
                $action = $_POST["action"];
                switch($action) {
                    case ('get'):
                        if (isset($request["tableName"]) && !empty($request["tableName"])) {
                            if (isset($request["sort"]) && !empty($request["sort"])) {
                                $sort = $request['sort'];
                            }
                            if (isset($request["filter"]) && !empty($request["filter"])) {
                                $filter = $request['filter'];
                            }
                            $this->getData($request["tableName"], $sort, $filter);
                        }
                        break;
                    case('set'):
                        if (isset($request["tableName"]) && !empty($request["tableName"])) {
                            if ((isset($request["data"]) && !empty($request["data"])) &&
                                (isset($request["idEditedData"]) && !empty($request["idEditedData"]))) {
                                $data = $request["data"];
                                $idEditedData = $request["idEditedData"];
                                $this->setData($request["tableName"], $data, $idEditedData);
                            }
                        }
                        break;
                }
            }
        }
    }

    public function getData($tableName, $sort = null, $filter = null) {
        $db = new Db();
        $fields = $db->getTableFields($tableName);
        $data['fields'] = $fields;
        $data += $db->getData($tableName, null, $sort, $filter);
        echo json_encode($data);
    }

    public function setData($tableName, array $data, array $idArray) {
        $db = new Db();
        $dataToInsert = $data;
        $fields = $dataToInsert['fields'];
        unset($dataToInsert['fields']);
        foreach ($dataToInsert as $row) {
            $row = array_combine($fields, $row);
            if(in_array($row['id'], $idArray)) {
                $db->updateData($tableName, $row['id'], [$fields[1] => $row['name'], $fields[2] => $row['phone_number']]);
            }
        }
        echo json_encode(["msg"=>"Operators data saved successfully!"]);
    }

    function is_ajax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}