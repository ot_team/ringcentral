<?php

namespace Api;
use Db\Db;

class ImportApi
{
    private $dbc;

    public function __construct(Db $dbc)
    {
        $this->dbc = $dbc;
    }

    public function getCallsLog() {
        $callsLogRecords = $this->dbc->getData('calls_log');
        return $callsLogRecords;
    }

    public function fillOperatorId() {
        $calls_log_keys = ['id','call_date', 'operator_id', 'inbound_number', 'outbound_number', 'duration'];
        $operators_keys = ['id','name','phone_number','calls_count_6','calls_count_24','calls_count_48','last_call_date'];
        $calls_log = $this->dbc->getData('calls_log');
        $calls_logDict = [];
        foreach ($calls_log as $call) {
            $calls_logDict[] = $this->createDictionary($calls_log_keys, $call);
        }
        $operators = $this->dbc->getData('operators');
        $operatorsDict = [];
        foreach ($operators as $operator) {
            $operatorsDict[] = $this->createDictionary($operators_keys, $operator);
        }
        $operatorsPhoneFirst = $this->operatorsPhoneFirstFormat($operatorsDict);
        $calls_logFinal = [];
        $timeArray = [];
        foreach ($calls_logDict as $call) {
            $call['operator_id'] = $operatorsPhoneFirst[$call['inbound_number']]['id'];
            $call['call_date'] = $this->getUnixTime($call['call_date']);
            $timeArray[] = $call['call_date'];
            $calls_logFinal[] = $call;
        }
        foreach ($calls_logFinal as $call) {
            $this->dbc->updateData('calls_log', $call['id'], ['operator_id' => $call['operator_id']]);
        }
        $countedIntervals = $this->getCountedIntervals(min($timeArray), max($timeArray));
        $countedOperators = $this->fillPhoneCount($operatorsDict, $calls_logFinal, $countedIntervals);
        foreach ($countedOperators as $operator) {
            $this->dbc->updateData('operators', $operator['id'], [
                    'calls_count_6' => $operator['calls_count_6'],
                    'calls_count_24' => $operator['calls_count_24'],
                    'calls_count_48' => $operator['calls_count_48'],
                    ]);
        }
    }

    /** INPUT --- 2017-11-27T01:05:32.816Z ---
     * @param $str_time
     * @return false|int
     */
    protected function getUnixTime($str_time) {
        $dateTime = preg_replace('#(.[0-9]+Z)#','',$str_time);
        $dateTime_array = explode('T', $dateTime);
        $time = explode(':', $dateTime_array[1]);
        $date = explode('-', $dateTime_array[0]);
        $unixTime = mktime((int)$time[0],(int)$time[1],(int)$time[2],(int)$date[1],(int)$date[2],(int)$date[0]);
        return $unixTime;
    }

    protected function convertUnixTimeToHours($unixTime) {
        return date('H:i:s Y-m-d', $unixTime);
    }

    protected function fillPhoneCount(array $operators, array $calls_log, array $intervals) {
        $operators = $this->operatorsIdFirstFormat($operators);
        foreach ($calls_log as $call) {
            $operators[$call['operator_id']]['calls_count_6'] = 0;
            $operators[$call['operator_id']]['calls_count_24'] = 0;
            $operators[$call['operator_id']]['calls_count_48'] = 0;
        }
        foreach ($calls_log as $call) {
            $call_date = $call['call_date'];
            if($call_date>=$intervals[6])
                $operators[$call['operator_id']]['calls_count_6']++;
            if($call_date>=$intervals[24])
                $operators[$call['operator_id']]['calls_count_24']++;
            if($call_date>=$intervals[48])
                $operators[$call['operator_id']]['calls_count_48']++;
        }
        return $operators;
    }

    protected function getCountedIntervals($min, $max) {
        $iter = [];
        for($i = $max,$j = 0;$i>=$min;$i-=60*60,$j++) {
            if($j<=6) $iter[6] = $i;
            elseif ($j<=24) $iter[24] = $i;
            elseif ($j<=48) $iter[48] = $i;
        }
        return $iter;
    }

    protected function sortByTime(array $array) {
        asort($array);
        return $array;
    }

    /** TODO: REFACTORING -> Concat next two functions
     * @param array $operators
     * @return array
     */
    protected function operatorsPhoneFirstFormat(array $operators) {
        $newOperatorsArray = [];
        $key = 'phone_number';
        foreach ($operators as $operator) {
            $operatorKey = '+'.$this->getFormatedPhoneNumber($operator[$key]);
            unset($operator[$key]);
            $newOperatorsArray[$operatorKey] = $operator;
        }
        return $newOperatorsArray;
    }

    protected function operatorsIdFirstFormat(array $operators) {
        $newOperatorsArray = [];
        $key = 'id';
        foreach ($operators as $operator) {
            $operatorKey = $operator[$key];
//            unset($operator[$key]);
            $newOperatorsArray[$operatorKey] = $operator;
        }
        return $newOperatorsArray;
    }

    public function createDictionary($keys, $values) {
        $newArray = [];
        for ($i=0;$i<count($keys);$i++) {
            $newArray[$keys[$i]] = $values[$i];
        }
        return $newArray;
    }

    public function getFormatedPhoneNumber($number) {
        return preg_replace('#[^0-9]#', '', $number);
    }

    /** Use after dropping table operators
     * @param array $calls_log
     */
    public function fillOperatorsTableMock(array $calls_log) {
        $names = ['David', 'Bruce', 'Carl', 'Piter', 'Lucy', 'Jessy', 'Maria'];
        $numbers = [];
        foreach ($calls_log as $record) {
            $currentNumber = $this->getFormatedPhoneNumber($record[3]);
            if(!in_array($currentNumber, $numbers)) {
                $numbers[] = $currentNumber;
            }
        }
        for ($i=0;$i<count($numbers);$i++) {
            $this->dbc->insertData('operators', ['name' => $this->getRandomName($names), 'phone_number' => "+".$numbers[$i]]);
        }
    }

    public function getRandomName(array $names) {
        return $names[rand(0, count($names)-1)];
    }
}