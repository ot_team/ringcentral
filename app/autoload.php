<?php

function AppAutoload($className) {
    $exploded = explode('\\', $className);
    switch ('app') {
        case 'app':
            require __DIR__ . '/' . implode(DIRECTORY_SEPARATOR, $exploded) . '.php';
            break;
    }
}

spl_autoload_register('AppAutoload', true, true);