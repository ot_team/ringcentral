var currentData = null,
    editedDataKeys = [],
    editedDataRows = [],
    editedData = null,
    dataSaved = true;

$(function () {

    getDataRequest('operators');

    $(".get_operators").click(function () {
        editedData = currentData;
        getDataRequest('operators');
    });

    $("#show-calls-log-table").click(function () {
        $(".calls-log-wrapper").css('display', 'block');
        getDataRequest('calls_log');
    });

    $("#hide-calls-log-table").click(function () {
        $(".calls-log-wrapper").css('display', 'none');
    });

    $(".get_callsLog").click(function () {
        getDataRequest('calls_log');
    });

    $("#update-operators").click(function () {
        setDataRequest();
    });

    $("#save-operators").click(function () {
        setDataRequest();
    });

    $('#cansel-saving').on('click', function () {
        $('.popup-wrapper').css('display', 'none');
    });

    $('#edit-cancel').on('click', function () {
        if (!dataSaved) {
            $('.popup-wrapper').css('display', 'block');
        } else {
            $("#exit-data-saved").click();
            goToControlPanel();
        }
    });

    $('#cansel-saving').on('click', function () {
        $('.popup-wrapper').css('display', 'none');
    });

    function goToControlPanel() {
        $("#edit-operators-form").submit();
    }

    function getDataRequest(tableName) {
        var data = {
            "action":"get",
            "tableName": tableName,
            "filter": {
                "phone_number": $("#filter-phone").val(),
                "name": $("#filter-name").val()
            }
        };

        data['sort'] = $("#sort").val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/app/Api/operators.php",
            data: data,
            success: function (data) {
                var rows = "", tableForRenderData;
                if (tableName === 'operators')
                    tableForRenderData = $(".operators-table");
                else if (tableName === 'calls_log')
                    tableForRenderData = $(".calls-log-table");

                currentData = data;
                combineData();
                rows = renderTable(currentData);
                tableForRenderData.empty();
                tableForRenderData.append(rows);
            },
            error: function () {
                alert("Sorry, but there are some issues with getting operators data.");
            }
        });
    }
    
    function setDataRequest() {
        var data = {
            "action":"set",
            "tableName":"operators",
            "idEditedData": editedDataKeys,
            "data": currentData
        };
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/app/Api/operators.php",
            data: data,
            success: function (data) {
                alert(data["msg"]);
                dataSaved = true;
            },
            error: function () {
                alert("Sorry, but there are some issues with saving operators data.");
            }
        });
    }
    
    function renderTable(data) {
        var rows = "", header = "<tr>", renderedTable="";
        $.each(data, function (row) {
            rows += "<tr>";
            if(row==='fields') {
                $.each(data[row], function (row, field) {
                    header += "<th>"+field+"</th>";
                })
            }
            else {
                $.each(data[row], function (row, value) {
                    rows += "<td>"+value+"</td>";
                })
            }
            header += "</tr>";
            rows += "</tr>";
            renderedTable = header+rows;
        });
        return renderedTable;
    }

    $("#filter-phone").keypress(function (e) {
        var keyCode = (e.which ? e.which : e.keyCode),
            keys = [48,49,50,51,52,53,54,55,56,57,58];
        if (keys.indexOf(keyCode) !== -1) {
            return true;
        }
        return false;
    });

    $("#filter-name").keypress(function (e) {
        var keyCode = (e.which ? e.which : e.keyCode),
            keys = [48,49,50,51,52,53,54,55,56,57,58];
        if (keys.indexOf(keyCode) !== -1) {
            return false;
        }
        return true;
    });

    $('#edit-operators-table').on('click', 'tr td', function () {
        var cell = $(this).index(),
        row = $(this).closest('tr').index(), value,
        input="<input id='cell-"+ row + "-" + cell +"' type='text' " +
            "class='form-control input-cell'>", inputElement = null, inputValue = null,
        cellElement = null, enterPressed = false;
        if(cell === 1 || cell === 2) {
            value = $(this).text();
            cellElement = $(this);
            $(this).empty();
            $(this).append(input);
            inputElement = $("#cell-"+row+"-"+cell);
            inputElement.focus();
            inputElement.val(value);
            inputElement.on('click', function () {
               return false;
            });
            $(this).on('keypress', function (e) {
                if(e.which === 13 && enterPressed === false ) {
                    enterPressed = true;
                    inputValue = inputElement.val();
                    inputElement.detach();
                    $(this).text(inputValue);
                    addDataToTempArray(row, cell, inputValue);
                }
            });
            inputElement.focusout(function () {
                if(enterPressed !== true) {
                    inputValue = $(this).val();
                    $(this).detach();
                    cellElement.text(inputValue);
                    addDataToTempArray(row, cell, inputValue);
                } else enterPressed = false;
            });
        }
    });

    function addDataToTempArray(row, column, value) {
        var keysAndRows = [];
        row -= 2;
        currentData[row][column] = value;
        editedDataKeys.push(currentData[row][0]);
        editedDataRows.push(row);
        dataSaved = false;
    }

    function combineData() {
        var rowIndex, keys = Array.from(editedDataKeys), rows = Array.from(editedDataRows);
        while (keys.length > 0) {
            for (i=0;i<Object.keys(currentData).length;i++) {
                if(keys.indexOf(currentData[i][0]) !== -1) {
                    rowIndex = rows[keys.indexOf(currentData[i][0])];
                    currentData[i] = editedData[rowIndex];
                    keys.splice(0, 1);
                    rows.splice(0, 1);
                    break;
                }
            }
        }
    }

});