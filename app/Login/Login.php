<?php
namespace Login;
use Db\Db;

class Login {

    private $db = null;
    private $message = [];

    public function login($name, $password)
    {
        $this->db = new Db();
        $username = $this->db->getEscapeString($name);
        $password = $this->db->getEscapeString($password);
        $result = $this->db->getData('users', ['username'=>$username]);
        if(!$this->db->rowsCount) $this->message['username'] = 'User with that name doesn\'t exist!';
        else {
            $user = array_combine(['id', 'username','password'], $result[0]);
            if(password_verify($password, $user['password'])) {
                $_SESSION['username'] = $user['username'];
                $_SESSION['logged_in'] = true;
            } else {
                $this->message['password'] = 'You have entered wrong password!';
            }
        }
        return $this->message;
    }
}