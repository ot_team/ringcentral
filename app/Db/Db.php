<?php

namespace Db;
//use \PDO;

class Db
{
    public $rowsCount;
    private static $DB_HOST = '';
    private static $DB_USER = '';
    private static $DB_PASSWORD = '';
    private static $DB_NAME = '';
    private $params = [];
    public $dbc = null;
//    private $pdo = null;

    public function __construct()
    {
        $config = include('config.php');
        $this->params = $config['db']['connection']['default'];
        $this::$DB_HOST = $this->params['host'];
        $this::$DB_USER = $this->params['username'];
        $this::$DB_PASSWORD = $this->params['password'];
        $this::$DB_NAME = $this->params['dbname'];
        //$this->connect();
//        $dsn = "mysql:host=". $this->params['host'] .";dbname=". $this->params['dbname'];
//        $this->pdo = new PDO($dsn ,$this::$DB_USER, $this::$DB_PASSWORD);
//        var_dump('ddd');

    }

    public function connect() {
        $this->dbc = mysqli_connect($this::$DB_HOST, $this::$DB_USER, $this::$DB_PASSWORD, $this::$DB_NAME);
    }

    /**
     * @param $tableName
     * @param array|null $data
     * @param null $sort (format => "fieldName-asc|desc")
     * @param null $filter
     * @return int
     */
    public function getData($tableName, array $data = null, $sort = null, $filter = null) {
        $this->connect();
        $cleanFilter = [];
        $query = "SELECT * FROM {$tableName}";
        if(isset($data)) $query .= " WHERE username = '".$data['username']."'";
        if($filter) {
            foreach ($filter as $column => $cond) {
                if($cond!=="") $cleanFilter[$column] = $cond;
            }
            if(count($cleanFilter)>=1) $query .= " WHERE ";
            foreach ($cleanFilter as $column => $cond) {
                if(strpos($query, "LIKE") !== false) {
                    $query .= "OR {$column} LIKE '%{$cond}%'";
                }
                else $query .= "{$column} LIKE '%{$cond}%' ";
            }
        }
        if($sort) {
            $sortArray = explode('-',$sort);
            $column = $sortArray[0];
            $sortType = $sortArray[1];
            $query .= " ORDER BY {$column} {$sortType}";
        }
        $result = $this->dbc->query($query);
        $this->rowsCount = $result->num_rows;
        $this->dbc->close();
        return $result==true?$result->fetch_all():0;
    }

    public function getTableFields($tableName) {
        $this->connect();
        $result = $this->dbc->query(
            "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{$tableName}'");
        $this->dbc->close();
        $result = $result->fetch_all();
        $cleanResult = [];
        foreach ($result as $item) {
            $cleanResult[] = $item[0];
        }
        return $cleanResult;
    }

    public function updateData($tableName, $id, $data) {
        $this->connect();
        $query = "UPDATE {$tableName} SET ";
        foreach ($data as $key => $value) {
            $query .= $key . " = '" . $value."', ";
        }
        $query = substr_replace($query, '', strlen($query)-2, 1);
        $id = is_string($id)? (string)$id : $id;
        $query .= " WHERE id = '". $id."'";
        $update = $this->dbc->query($query);
        if(!$update) {
            print_r('ERROR: '.$this->dbc->error.'<br>');
        }
        $this->dbc->close();
    }

    public function insertData($tableName, array $data = null) {
        $this->connect();
        $query_left = "INSERT INTO {$tableName} (";
        $query_right = "VALUES (";
        foreach ($data as $key => $value) {
            $query_left .= $key . ', ';
            if(is_string($value)) $query_right .= "'".$value."', ";
                else $query_right .= $value;
        }
        $query_left = substr_replace($query_left, '', strlen($query_left)-2, 1);
        $query_right = substr_replace($query_right, '', strlen($query_right)-2, 1);
        $query_left .= ") ";
        $query_right .= ") ";
        $finalQuery = $query_left . $query_right;
//        try {
//            $insert = $this->dbc->query($finalQuery);
//        } catch (\Exception $e) {
//            var_dump($e);
//        }
        $insert = $this->dbc->query($finalQuery);
        if(!$insert) {
            var_dump($this->dbc->error);
            print_r('ERROR: '.$this->dbc->error);
        }
//        $stm = $this->pdo->prepare($finalQuery);
//        $stm->execute();
        $this->dbc->close();
    }

    public function getEscapeString($string) {
        $this->connect();
        $escapeString = $this->dbc->escape_string($string);
        $this->dbc->close();
        return $escapeString;
    }
}