<?php
return array(
    'db' => array(
        'connection' => array(
            'default' => array(
                'host' => 'localhost',
                'dbname' => 'ringcentral',
                'username' => 'root',
                'password' => 'root'
            ),
        ),
    ),
);