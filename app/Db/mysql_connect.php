<?php

define('DB_HOST', $db_connection['host']);
define('DB_USER', $db_connection['username']);
define('DB_PASSWORD', $db_connection['password']);
define('DB_NAME', $db_connection['dbname']);

$dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if(!$dbc) {
    $dbc_first = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if(!$dbc_first) {
        die('Could not connect to MySQL: '. mysqli_connect_error());
    } else {
        $dbCreateQuery = "CREATE DATABASE {$db_connection['dbname']}";
        if(!mysqli_query($dbc_first, $dbCreateQuery)) {
            echo 'Database create error: '. mysqli_error($dbc_first);
        } else {
            $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        }
        mysqli_close($dbc_first);
    }
}
require_once('mysql_prepare.php');
mysqli_close($dbc);