<?php

$operators_query = "CREATE TABLE IF NOT EXISTS operators(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(45) NOT NULL, 
phone_number text NOT NULL, 
calls_count_6 int NULL DEFAULT 0, 
calls_count_24 int NULL DEFAULT 0, 
calls_count_48 int NULL DEFAULT 0,
last_call_date text NULL,
PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$calls_log_query = "CREATE TABLE IF NOT EXISTS calls_log(
id varchar(50) NOT NULL,
call_date text NOT NULL, 
operator_id int(11) NOT NULL, 
inbound_number text NOT NULL, 
outbound_number text NOT NULL, 
duration int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$user_query = "CREATE TABLE IF NOT EXISTS users(
id INT NOT NULL AUTO_INCREMENT,
username VARCHAR(50) NOT NULL,
password VARCHAR(100) NULL, 
PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";

mysqli_query($dbc, $calls_log_query);
mysqli_query($dbc, $operators_query);
mysqli_query($dbc, $user_query);