<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['log-out'])) {
        session_unset();
        header("location:index.php");
        exit;
}
?>
<div class="log-out-form">
    <form action="" method="post">
        <button type="submit" class="btn btn-danger" id="log-out" name="log-out">
            Logout <span class="fa fa-sign-out" aria-hidden="true"></span></button>
    </form>
</div>



