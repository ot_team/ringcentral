<?php
namespace view;
use Db\Db;
use Login\Login;

session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $login = new Login();
        $loginMessage = $login->login($username, $password);
        if(!isset($loginMessage['username']) && !isset($loginMessage['password']))
        {
            header("location:index.php");
            exit;
        }
    }
}

?>
<?php
require_once('header.php'); ?>
<div class="table-wrapper">
    <div class="table-cell-wrapper">
        <div class="container">
            <div class="row col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <form action="" method="post" autocomplete="off">
                        <div class="panel-heading">
                            <h4>Please, enter your login and password</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="username">
                                    Login: <span class="text-danger"><?= isset($loginMessage['username'])?$loginMessage['username']:'' ?></span></label>
                                <input type="text" name="username" id="username" class="form-control"
                                    <?= isset($loginMessage)?"value='{$_POST['username']}'":'';?>>
                            </div>
                            <div class="form-group">
                                <label for="password">
                                    Password: <span class="text-danger"><?= isset($loginMessage['password'])?$loginMessage['password']:'' ?></span></label>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                        </div>
                        <div class="panel-footer">
                            <input type="submit" name="login" class="btn btn-success" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php');