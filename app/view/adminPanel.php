<?php
$db = new \Db\Db();
$api = new \Api\ImportApi($db);

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['log-out'])) {
        session_unset();
        header("location:index.php");
        exit;
    }
    if(isset($_POST['create-operators-table'])) {
        $db->getData('calls_log');
        if($db->rowsCount > 0) {
            $db->getData('operators');
            if($db->rowsCount == 0) $api->fillOperatorsTableMock($api->getCallsLog());
        }
    }
    if(isset($_POST['fill-operators-id-calculate-calls-count'])) {
        $db->getData('operators');
        if($db->rowsCount > 0) {
            $db->getData('calls_log');
            if($db->rowsCount > 0) {
                $api->fillOperatorId();
            }
        }
    }
}
?>
<div class="admin-panel row">
    <div class="panel panel-default col-md-12">
        <div class="panel-body">
            <div class="prepare-data-form">
                <form action="" method="post">
                    <button type="submit" class="btn btn-success"
                            id="create-operators-table" name="create-operators-table">
                        <span class="fa fa-plus" aria-hidden="true">
                        </span> Generate operators table</button>
                        <button type="submit" class="btn btn-success"
                                id="fill-operators-id-calculate-calls-count"
                                name="fill-operators-id-calculate-calls-count">
                        <span class="fa fa-cog" aria-hidden="true">
                        </span> Fill operators id and calculate calls count</button>
                </form>
            </div>
            <div class="show-callsLog-table">
                    <button type="submit" class="btn btn-success"
                            id="show-calls-log-table" name="show-calls-log-table">
                        <span class="fa fa-eye" aria-hidden="true">
                        </span> Show calls log table</button>
            </div>
            <div class="hide-callsLog-table">
                <button type="submit" class="btn btn-warning"
                        id="hide-calls-log-table" name="hide-calls-log-table">
                        <span class="fa fa-eye" aria-hidden="true">
                        </span> Hide calls log table</button>
            </div>
            <div class="log-out-form">
                <form action="" method="post">
                    <button type="submit" class="btn btn-danger" id="log-out" name="log-out">
                        <span class="fa fa-sign-out" aria-hidden="true"></span> Logout</button>
                </form>
            </div>
        </div>
    </div>
</div>