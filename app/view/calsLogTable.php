<div class="row col-md-12 calls-log-wrapper">
    <div class="panel panel-default" style="overflow: hidden;">
        <div class="panel-heading row">
            <h4>Calls Log data</h4>
        </div>
        <div class="panel-body">
            <table class="table table-striped calls-log-table">
            </table>

        </div>
        <div class="panel-footer col-md-12">
            <button type="submit"
                    class="btn btn-success get_callsLog">
                            <span class="fa fa-download" aria-hidden="true">
                            </span>
                Get calls log
            </button>
        </div>
    </div>
</div>