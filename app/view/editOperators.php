<?php
namespace view;

session_start();
?>
    <?php
require_once('header.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row col-md-10 col-md-offset-1">
                <div class="panel panel-default" style="overflow: hidden;">
                    <div class="panel-heading row">
                        <form action="" method="post" id="edit-operators-form">
                            <div class="form-field col-md-3">
                                <label for="sort">Sorting: </label>
                                <select name="sort" id="sort" class="form-control">
                                    <option value="id-asc">Asc by id</option>
                                    <option value="id-desc">Desc by id</option>
                                    <option value="name-asc">Asc by name</option>
                                    <option value="name-desc">Desc by name</option>
                                </select>
                            </div>
                            <div class="form-field col-md-3">
                                <label for="filter-phone"> By phone number (filter):</label>
                                <input type="text"
                                       name="filter-phone"
                                       id="filter-phone"
                                       class="form-control">
                            </div>
                            <div class="form-field col-md-3">
                                <label for="filter-name"> By name (filter):</label>
                                <input type="text"
                                       name="filter-name"
                                       id="filter-name"
                                       class="form-control">
                            </div>
                            <div class="form-field col-md-3">
                                <input type="hidden" id="exit-data-saved" name="edit-operators-cancel"/>
                                <a      name="edit-cancel"
                                        id="edit-cancel"
                                        class="btn btn-danger form-control">
                                        <span class="fa fa-ban" aria-hidden="true">
                                        </span>
                                    Cancel
                                </a>
                                <button type="button"
                                        name="update-operators"
                                        id="update-operators"
                                        class="btn btn-success form-control">
                                        <span class="fa fa-pencil-square-o" aria-hidden="true">
                                        </span>
                                    Save
                                </button>

                            </div>

                        </form>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped operators-table" id="edit-operators-table">
                        </table>

                    </div>
                    <div class="panel-footer col-md-12">
                        <button type="submit"
                                class="btn btn-success get_operators">
                            <span class="fa fa-download" aria-hidden="true">
                            </span>
                            Get operators
                        </button>
                    </div>
                </div>
            </div>
            <?php require_once("calsLogTable.php"); ?>
        </div>
    </div>
<?php require_once('footer.php');