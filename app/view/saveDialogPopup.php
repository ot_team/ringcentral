<div class="popup-wrapper row">
    <div class="popup col-md-4 col-md-offset-4">
        <form action="" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-warning">Warning!!!</h4>
                </div>
                <div class="panel-body">
                    <p class="text-danger">Do you want to save edited data?</p>
                </div>
                <div class="panel-footer">
                    <button type="submit"
                            name="save-operators"
                            id="save-operators"
                            class="btn btn-success save_operators">
                        Yes
                    </button>
                    <button type="submit"
                            name="edit-operators-cancel"
                            id="edit-operators-cancel"
                            class="btn btn-danger save_operators">
                        No
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
