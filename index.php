<?php

require_once('app/bootstrap.php');
require_once('app/index.php');

use Db\Db;
$db = new Db();
$api = new \Api\ImportApi($db);

$db->getData('users');
if($db->rowsCount == 0) $db->insertData('users',
    ['username'=>$db->getEscapeString('admin'),
        'password'=>$db->getEscapeString(password_hash('secret', PASSWORD_BCRYPT))]);
